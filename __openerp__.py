# -*- coding: utf-8 -*-
{
    'name': "Poultry Farm Manager",

    'summary': """
        A New Era Application for Poultary Farm Management
        """,

    'description': """
        This Module will provide Broiler Management System, Layer Management System
        Vaccination Management System and Sales Management
    """,

    'author': "Shaheryar Malik",
    'website': "http://www.shaheryarmalik.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/breed_view.xml',
        'views/farm_view.xml',
        'views/broiler_view.xml',
        'views/vaccine_view.xml',
        'views/layer_view.xml',
        'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}