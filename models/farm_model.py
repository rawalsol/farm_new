# -*- coding = utf-8 -*-

from openerp import models, fields, api

class Farm(models.Model):
    _name = 'farm.farm'


    farm_id = fields.Char('Farm ID')
    farm_name = fields.Char('Farm Name')
    farm_address = fields.Text('Farm Address')

    #This is a computed field for farm capacity it is equal to total of sheds capacity
    farm_capacity = fields.Integer(string='Farm Capacity', store=True, compute='_totalcapacity')

    #todo put the field from res.partner here
    farm_supervisor = fields.Char('Supervisor')
    farm_phone = fields.Char('Phone #')
    farm_mobile = fields.Char('Mobile #')

    # Define number of Sheds as one Farm can have many sheds
    farm_sheds = fields.One2many('farm.shed', 'shed_ids')

    @api.one
    @api.depends('farm_sheds.shed_capacity')
    def _totalcapacity(self):
        total = 0
        for capacity in self.farm_sheds:
            total = total + capacity.shed_capacity
        self.farm_capacity = total

    # This method will show the Farm Names when this model will
    # Be called using Many2one related fields
    # @api.multi
    # @api.depends('farm_id')
    # def name_get(self):
    #     result = []
    #     for names in self:
    #         list = names.farm_name
    #         result.append((names.farm_id, '%s' % (list)))
    #     return result

class Sheds(models.Model):
    _name='farm.shed'

    shed_ids = fields.Many2one('farm.farm', 'Main Farm')
    shed_id = fields.Char('Shed ID')
    shed_name = fields.Char('Shed Name')
    shed_capacity = fields.Integer('Shed Capacity')

    # @api.multi
    # @api.depends('shed_id')
    # def name_get(self):
    #     result = []
    #     for names in self:
    #         list = names.shed_name
    #         result.append((names.shed_id, '%s' % (list)))
    #     return result

