# -*- coding = utf-8 -*-
from openerp import models, fields, api

class Breeds(models.Model):
    _name='farm.breeds'

    breed_id = fields.Char('Breed ID')
    breed_name = fields.Char('Breed Name')
    description = fields.Text('Description')



    #over-riding the display name show Breed Name Instead
    # @api.multi
    # @api.depends('breed_name')
    # def name_get(self):
    #     result = []
    #     for names in self:
    #         list = names.breed_name
    #         result.append((names.breed_id,'%s' % (list)))
    #     return result

    #TODO the Breed must be unique and the following constraint should be added
    # _sql_constraints = [
    #     ('todo_task_name_uniq',
    #      'UNIQUE (name, user_id, active)',
    #      'Task title must be unique!')]