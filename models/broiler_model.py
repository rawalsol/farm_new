# -*- coding = utf-8 -*-

from openerp import models, fields, api

#Todo Add security csv files for user access

class Broiler(models.Model):
    _name = 'farm.broiler'

    start_date = fields.Date('Date')
    age = fields.Integer('Age (weeks)')
    breed = fields.Many2one('farm.breeds', 'Breed')
    flock = fields.Many2one('farm.broiler.flock', 'Flock')
    mortality = fields.Integer('Mortality')
    cull = fields.Integer('Cull')
    feed = fields.Many2one('farm.feed', 'Feed')
    feed_consumption = fields.Integer('Feed Consumption (kg\'s)')
    water = fields.Integer('Water (ltr)')
    remarks = fields.Text('Remarks')
    vaccine = fields.Many2one('farm.vaccine', 'Vaccine')


    # FOLLOWING CODE IS SUITABLE FOR SUPERVISOR FIELD
    # # class TodoTask(models.Model):
    # refers_to = fields.Reference(
    #     [('res.user', 'User'), ('res.partner', 'Partner')],
    #     'Refers to')


class Flock(models.Model):
    _name = 'farm.broiler.flock'

    farm_code = fields.Many2one('farm.farm', 'Farm Code')
    shed_code = fields.Char(related='farm_code.farm_sheds.shed_id', string='Shed Code')

class Feed(models.Model):
    _name = 'farm.feed'

    feed = fields.Char('Feed')



    # @api.onchange('farm_code')  # if these fields are changed, call method
    # def _check_change(self):
    #     self.shed_code = self.farm_code.farm_sheds


# #Todo Add Constrains to Flock ID etc
#     @api.one
#     @api.constrains('farm_code')
#     def _select_shed(self):
#         if len(self.name) < 5:
#             raise ValidationError('Must have 5 chars!')
#     @api.model
#     def name_get(self):
#         result = []
#         for book in self:
#             authors = book.author_ids.mapped('name')
#             name = u'%s (%s)' % (book.title, u', '.join(authors))
#             result.append((book.id, name))
#         return result

    # @api.model
    # def name_get(self):
    #     result = []
    #     for shed in self:
    #         shedlist = shed.farm_code.mapped('farm_shed')
    #         name = u'%s' % (shedlist)
    #         result.append((name))
    #     return result

