# -*- coding: utf-8 -*-

from openerp import models, fields, api


class farm_new(models.Model):
     _name = 'farm.transfer'

     date = fields.Date('Date')
     fromshed = fields.Many2one('farm.shed','From Shed')
     flock = fields.Many2one('farm.layer', 'Flock')
     birdsavailable = fields.Integer('No. of Birds')
     toshed = fields.Many2one('farm.layer','To Shed')
    
     description = fields.Text()

#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100