# -*- coding = utf-8 -*-

from openerp import models, fields, api

class Vaccine(models.Model):
    _name = 'farm.vaccine'

    vaccine_id = fields.Char('Vaccine ID')
    vaccine_name = fields.Char('Vaccine Name')
    vaccine_description = fields.Char('Description')