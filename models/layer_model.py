# -*- coding = utf-8 -*-

from openerp import models, fields, api

class Layer(models.Model):
    _name = 'farm.layer'

    
    farmcode = fields.Many2one('farm.farm','Farm Code')
    unitcode = fields.Char('Unit Code')
    shedcode = fields.Many2one('farm.shed', 'Shed Code')
    flockcode = fields.Char(string='flock Code', store=True, compute='_flockcode')

    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')

    age = fields.Integer('Age')
    breed = fields.Many2one('farm.breeds', 'Breed')
    openingbirds = fields.Integer('Opening Birds')



    @api.multi
    @api.depends('farmcode','unitcode','shedcode')
    def _flockcode(self):

    	code = str(self.farmcode) + "-" + str(self.unitcode) + "-"  + str(self.shedcode)
        self.flockcode = code


class LayerEntry(models.Model):

	_name = 'farm.layer.daily'
	# flock = fields.Many2one(string="Flock Code", store=True, compute='_flockcodelist')
	
	date = fields.Date('Date')
	flock = fields.Many2one('farm.layer','flockcode')
	
	age = fields.Integer('Age')
	mort = fields.Integer('Mortality')
	culls = fields.Integer('Culls')
	feed = fields.Many2one('farm.feed','Feed')
	qty = fields.Integer('Feet Qty (kg\s)')

	# def _flockcodelist(self):
	# 	result = []
	# 	for names in farm.layer:
	# 		list = names.flockcode
	# 		result.append((names.flockcode, '%s' % (list)))
	# 	return result 
				
